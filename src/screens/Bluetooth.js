import React from 'react';
import {Text, View} from "react-native";
import Switch from "../components/Switch/Switch"
import mainStyles from "../assets/styles/main-styles"

const Bluetooth = () => {

  return(
    <View  style={mainStyles.container}>
      <Switch/>
    </View>
  )
}

export default Bluetooth;
