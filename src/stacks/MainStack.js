import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import CustomTopBar from "../components/CustomTopBar/CustomTopBar"
import Start from "../screens/Start"
import Bluetooth from "../screens/Bluetooth"
import Test from "../screens/Test"
const Stack = createStackNavigator()

export default () => {
  return (
    <Stack.Navigator
      initialRouteName="Bluetooth"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name="Start"
        component={Start} />
      <Stack.Screen
        name="Bluetooth"
        component={Bluetooth}
        options={{
          header: props => <CustomTopBar {...props}
                                         label={['Поиск', 'устройства']}
                                         typeImage="back"
          />,
          headerShown: true,
        }}
      />
      <Stack.Screen name="Test" component={Test} />
    </Stack.Navigator>
  )
}
