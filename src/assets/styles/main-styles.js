'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    position: 'relative',
    flex: 1,
    backgroundColor: '#fff',
    zIndex: 1
  },
  btnAquamarine:{
    backgroundColor: '#00B9C5',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 30,
    paddingRight: 25,
    paddingLeft: 25,
    paddingBottom: 10,
    paddingTop: 10
  },
  btnAquamarineText:{
    fontFamily: 'OpenSans-Bold',
    fontSize: 18,
    lineHeight: 25,
    color: '#fff',
  },
  Switcher:{
    width: 50,
    height: 25,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#97A1B3',
    position: 'relative'
  },
  SwitcherBall:{
    backgroundColor: '#C5CBD9',
    width: 16,
    height: 16,
    borderRadius: 8,
    position: 'absolute',
    left: 5,
    top: 3
  },
});
