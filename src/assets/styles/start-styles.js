'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  imgIndicator:{
    position: 'absolute',
    bottom: 47,
    left: 0,
    zIndex: 5,
    width: 240,
    height: 126
  },
  imgBackground:{
    position: 'absolute',
    width: '100%',
    height: 205,
    bottom: -5,
    left: 0,
  },
  imgLogo:{
    position: 'relative',
    width: 200,
    height: 40,
  },
  title:{
    color: '#2F4267',
    marginTop: 30,
    marginBottom: 25,
    fontFamily: 'OpenSans-Bold',
    fontWeight: '600',
    fontSize: 18,
    lineHeight: 21,
    textTransform: 'uppercase'
  },
  text:{
    color: '#2F4267',
    textAlign: 'center',
    paddingLeft: 50,
    paddingRight: 50,
    marginBottom: 35,
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    lineHeight: 21,
    fontWeight: '400',
  }
});
