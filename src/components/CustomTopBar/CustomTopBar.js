import React from 'react';
import {StyleSheet, Text, View, Image, Button, TouchableOpacity} from "react-native"
// import LinearGradient from "react-native-linear-gradient";
import customTopBarStyles from "./custom-top-bar-styles"
import {LinearGradient} from 'expo-linear-gradient'
import BackIcon from "../../assets/images/back_icon.svg"

const CustomTopBar = ({navigation, label, typeImage}) => {

  return (
    <View style={{backgroundColor: '#eee'}}>
      <LinearGradient
        colors={['#49B869', '#34B883', '#1EB9A0', '#0DB9B4', '#03B9C1', '#00B9C5']}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        style={{height: 50}}
      >
        <View style={customTopBarStyles.customTabBarArea}>
          {typeImage === "back"
          &&
          <View style={customTopBarStyles.buttonBack}
                onPress={e => navigation.navigate('Start')}>
            {/*<BackIcon width="9" height="15"/>*/}
          </View>
          }
          <Text style={[customTopBarStyles.customTabBarAreaText, {fontWeight: 'bold'}]}>
            {label[0]}&nbsp;
          </Text>

          <Text style={[customTopBarStyles.customTabBarAreaText]}>
            {label[1]}
          </Text>
        </View>
      </LinearGradient>
    </View>
  );
};

export default CustomTopBar;
