import {StyleSheet, Text, View} from "react-native";
import React from 'react';
import {useFonts} from 'expo-font';
import MainStack from './src/stacks/MainStack'
import {NavigationContainer} from '@react-navigation/native';

export default function App() {

  let [fontsLoaded] = useFonts(
    {
      'OpenSans-Regular': require('./src/assets/fonts/OpenSans-Regular.ttf'),
    },
    {
      'OpenSans-Bold': require('./src/assets/fonts/OpenSans-Bold.ttf'),
    }
  );

  return (
    <NavigationContainer>
      <MainStack/>
    </NavigationContainer>
  )
}

